package ru.kuzin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.model.IWBS;
import ru.kuzin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @NotNull
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created")
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Override
    public String toString() {
        return this.getName() + " | Status: " + status.getDisplayName() + " |";
    }

}